package com.example.kecapsocket;

import io.socket.IOAcknowledge;
import io.socket.IOCallback;
import io.socket.SocketIO;
import io.socket.SocketIOException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	public TextView myTextView;
	public EditText myTextField;
	public Button myButton;
	private MainActivity _myActivity;
	final String TAG = "Kecap Socket";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		myTextView = (TextView) findViewById(R.id.myTextView);
		myTextField = (EditText) findViewById(R.id.myTextField);
		myButton = (Button) findViewById(R.id.myButton);
		myTextView.setMovementMethod(new ScrollingMovementMethod());

		myTextView.setText("Ready :D\n");
		myButton.setOnClickListener(myHandler);
		_myActivity = this;
	}

	View.OnClickListener myHandler = new View.OnClickListener() {
		public void onClick(View v) {
			// try {
			// KecapSock(myTextField.getText().toString());
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			new SocketTask().execute(_myActivity.myTextField.getText()
					.toString());
		}
	};

	private class SocketTask extends AsyncTask<String, String, String> {
		@Override
		protected void onProgressUpdate(String... progress) {
			Log.d(TAG, progress[0].toString());
			_myActivity.myTextView.append(progress[0].toString());
		}

		@Override
		protected void onPostExecute(String result) {
			Log.d(TAG, result);
			_myActivity.myTextView.append(result);
		}

		@Override
		protected String doInBackground(String... params) {
			publishProgress("Trying to connect to " + params[0] + '\n');
			try {
				SocketIO socket = new SocketIO(params[0].toString());
				socket.connect(new IOCallback() {
					@Override
					public void onMessage(JSONObject json, IOAcknowledge ack) {
						try {
							JSONObject data = json.getJSONObject("data");
							Log.d(TAG,
									data.get("name") + " ==> "
											+ data.get("status") + '\n');
							publishProgress(data.get("name") + " ==> "
									+ data.get("status") + '\n');
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onMessage(String data, IOAcknowledge ack) {
					}

					@Override
					public void onError(SocketIOException socketIOException) {
						Log.d(TAG, "an Error occured\n");
						publishProgress("an Error occured\n");
						socketIOException.printStackTrace();
					}

					@Override
					public void onDisconnect() {
						Log.d(TAG, "Connection terminated.\n");
						publishProgress("Connection terminated.\n");
					}

					@Override
					public void onConnect() {
						Log.d(TAG, "Connection established\n");
						publishProgress("Connection established\n");
					}

					@Override
					public void on(String event, IOAcknowledge ack,
							Object... args) {
						Log.d(TAG, "Server triggered event '" + event + "'\n");
						publishProgress("Server triggered event '" + event + "'\n");
					}
				});

				IOAcknowledge ioAck = new IOAcknowledge() {
					@Override
					public void ack(Object... args) {
						Log.d(TAG, "Server acknowledges this package.\n");
						publishProgress("Server acknowledges this package.\n");
					}
				};

				JSONObject json = new JSONObject();
				json.put("url", "/power");
				socket.emit("message", ioAck, json.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "AsyncTask Begin\n";
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void KecapSock(String strCon) throws Exception {

	}
}
